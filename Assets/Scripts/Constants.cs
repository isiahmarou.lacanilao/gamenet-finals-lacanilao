public class Constants
{
    public const string PLAYER_READY = "isPlayerReady";
    public const string PLAYER_SELECTION_NUMBER = "playerSelectionNumber";
    
    public enum RaiseEventCode
    {
        PlayerDeath = 1,
        PlayerRespawn = 2,
    }
}
